from tkinter import *


class Calculette():

    def __init__(self):
        # création de l'objet fenêtre principal
        fenetre = Tk()

        # Ajout d'un titre en haut de la fenêtre graphique
        fenetre.title("Calculette Eco-déplacements")

        # Création d'un élément graphique permettant de dessiner à l'intérieur
        canevas = Canvas(fenetre, width=100, height=100)

        # Chargement d'une image et affichage en position (0,0)
        logo_tux = PhotoImage(file="logo_tux.gif")
        img_logo = canevas.create_image(0, 0, anchor=NW, image=logo_tux)

        # Positionnement effectif du canevas dans la fenêtre graphique
        canevas.pack()

        # Création des en-têtes
        en_tete_1 = Label(fenetre, text="Calculette Eco-déplacements")
        en_tete_2 = Label(fenetre, text="""\
            Calculez l'impact de vos déplacements quotidiens""")
        en_tete_3 = Label(fenetre, text="sur l'environnement et vos dépenses")

        # Positionnement de l'en-tête dans la fenêtre graphique
        en_tete_1.pack(side=TOP)
        en_tete_2.pack(side=RIGHT)
        en_tete_3.pack(side=RIGHT)

        # Lancement de la boucle événementielle
        fenetre.mainloop()


if __name__ == "__main__":
    cal = Calculette()
