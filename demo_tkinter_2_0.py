from tkinter import *


class Calculette():

    def __init__(self):
        # création de l'objet fenêtre principal
        fenetre = Tk()

        # Ajout d'un titre en haut de la fenêtre graphique
        fenetre.title("Calculette Eco-déplacements")

        # Création d'un élément graphique permettant de dessiner à l'intérieur
        canevas = Canvas(fenetre, width=100, height=100)

        # Chargement d'une image et affichage en position (0,0)
        logo_tux = PhotoImage(file="logo_tux.gif")
        img_logo = canevas.create_image(0, 0, anchor=NW, image=logo_tux)

        # Positionnement effectif du canevas dans la fenêtre graphique
        canevas.pack()

        # Lancement de la boucle événementielle
        fenetre.mainloop()


if __name__ == "__main__":
    cal = Calculette()
