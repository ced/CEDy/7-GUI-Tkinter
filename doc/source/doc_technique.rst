Documentation technique
=======================

.. automodule:: calculette
.. autoclass:: Calculette
    :members:
    :private-members:
    :special-members:
