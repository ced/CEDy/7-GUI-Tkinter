.. CEDy documentation master file, created by
   sphinx-quickstart on Wed Jan  7 08:03:27 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvevue sur la page de documentation du projet CEDy
============================================================

C'est la page d'accueil du projet CEDy. Ce projet contient :
    * La documentation Sphinx
    * Des fichiers RST
    * Les fichiers Python


Documentation
-------------

.. toctree::
   :maxdepth: 2
   
   doc_technique
