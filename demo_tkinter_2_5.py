from tkinter import *


class Calculette():

    def __init__(self):
        # création de l'objet fenêtre principal
        fenetre = Tk()

        # Ajout d'un titre en haut de la fenêtre graphique
        fenetre.title("Calculette Eco-déplacements")

        # Création d'un élément graphique permettant de dessiner à l'intérieur
        canevas = Canvas(fenetre, width=100, height=100)

        # Chargement d'une image et affichage en position (0,0)
        logo_tux = PhotoImage(file="logo_tux.gif")
        img_logo = canevas.create_image(0, 0, anchor=NW, image=logo_tux)

        # création d'un cadre pour l'en-tête
        cadre = Frame(fenetre, width=200, height=100)

        # Création des en-têtes
        en_tete_1 = Label(cadre, text="Calculette Eco-déplacements")
        en_tete_2 = Label(cadre, text="""\
            Calculez l'impact de vos déplacements quotidiens""")
        en_tete_3 = Label(cadre, text="sur l'environnement et vos dépenses")

        # positionnement des en-têtes dans le cadre
        en_tete_1.pack(padx=10, pady=10)
        en_tete_2.pack()
        en_tete_3.pack()

        # Définition de la distance
        txt_distance = Label(fenetre, text="""\
            Quelle distance entre le domicile et le lieu de travail ?""")
        entr_distance = Entry(fenetre)

        # Définition des modes de déplacement
        txt_dep_1 = Label(fenetre, text="""\
            Choisir le premier mode de déplacement :""")
        txt_dep_2 = Label(fenetre, text="""\
            Choisir le second mode de déplacement :""")

        # Création des Listbox
        # l'option exportselection à False permet de ne pas exporter la
        # sélection au serveur X et ainsi de pouvoir gérer plusieurs Listbox
        list_dep_1 = Listbox(fenetre, exportselection=False)
        list_dep_2 = Listbox(fenetre, exportselection=False)
        list_dep_1.insert(1, "train")
        list_dep_1.insert(2, "velo")
        list_dep_1.insert(3, "voiture")
        list_dep_2.insert(1, "train")
        list_dep_2.insert(2, "velo")
        list_dep_2.insert(3, "voiture")

        # Mise en page de la fenêtre graphique
        canevas.grid(row=1, column=1, padx=10, pady=10)
        cadre.grid(row=1, column=2, padx=10, pady=10)
        txt_distance.grid(row=2, column=1, padx=10, pady=10)
        entr_distance.grid(row=2, column=2, padx=10, pady=10)
        list_dep_1.grid(row=3, column=1, padx=10, pady=10)
        list_dep_2.grid(row=3, column=2, padx=10, pady=10)
        list_dep_1.grid(row=4, column=1, padx=10, pady=1)
        list_dep_2.grid(row=4, column=2, padx=10, pady=1)

        # Lancement de la boucle événementielle
        fenetre.mainloop()


if __name__ == "__main__":
    cal = Calculette()
