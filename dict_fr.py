# Définition variables globales
dico_lib = {
    'titre': ("Calculette Eco-déplacements",
              "Calculez l'impact de vos déplacements quotidiens",
              "sur l'environnement et vos dépenses"),
    'distance': """Quelle est la distance entre le domicile et le lieu \
de travail ?""",
    'deplacement': "Choisir le {} mode de déplacement :",
    'menu': ("le train", "le vélo", "la voiture"),
    1: "premier",
    2: "second",
    'choix_1': 'Je choisis {}',
    'choix_2': 'plutôt que {}',
    'IOError': "Impossible de lire le fichier {}.txt!",
    'ValueError': "Veuillez saisir une valeur entière pour la distance !",
    'IndexError': "Veuillez sélectionner un mode de déplacement !",
    'TclError': "Erreur Tcl : Vérifiez la présence du fichier logo !",
    'valide': "Calculer",
    'hauteur': 400,
    'largeur': 400,
}
dico_cout = {
    0: (14.62, 9.26),
    1: (0, 0),
    2: (129.62, 50.65),
    'lab_titres': ("Effet de serre", "Énergie"),
    'lab_unites': ("{} kg eq. CO2", "{} l eq. pétrole"),
    'lab_eco_1': ("J'évite {} kg", "Je consomme {} litres"),
    'lab_eco_2': ("eq. CO2 par an", "eq. pétrole en moins par an"),
    'lab_dep_1': ("J'émets {} kg", "Je consomme {} litres"),
    'lab_dep_2': ("eq. CO2 en plus par an.", "eq. pétrole en plus par an."),
}
