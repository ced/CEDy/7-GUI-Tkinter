Voici une nouvelle itération libre de la calculette de l'Ademe.
Celle-ci repose sur une interface graphique (GUI : *Graphic User Interface*).

## Notre mission

Il existe de nombreux modules Python permettant de créer des interfaces graphiques.
Nous utiliserons ici un environnement graphique très simple et installé par défaut
sur tous les systèmes : Tk.

Le module **Tkinter** que nous allons utiliser, repose sur la programmation orientée objet.
Ce style de programmation est certes peu plus complexe, mais il possède une grande qualité :
le code est bien structuré et facilement réutilisable...

> Cette version est pour le moment limitée. En effet, une exception est levée
si on valide l'application sans avoir renseigner une distance ou si la valeur
saisie n'est pas entière.
De même si aucun élément n'a été sélectionné dans l'une ou l'autre des listes de choix,
une autre exception est levée.
Ces différents éléments seront traiter dans le cadre d'un exercice d'application.

## Documentation technique

L'application CEDy dispose d'une documentation technique qui a été
générée avec Sphinx, elle est accessible [en ligne][doc]
(via les pages de framagit et l'intégration continue).

[doc]: http://ced.frama.io/CEDy/7-GUI-Tkinter
