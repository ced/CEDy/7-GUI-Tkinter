"""
    :copyright: (c) 2014 par moulinux.
    :license: GPLv3, voir la LICENCE pour plus d'informations.
"""
from tkinter import *


class Calculette():
    """
    Définition d'une calculette eco-déplacements, permettant de
    calculer l'impact de nos déplacements quotidiens
    sur l'environnement et nos dépenses.
    """
    def __init__(self, dico, cout_km):
        """
        Constructeur de la classe.

        dico : dictionnaire contenant la définition des en-têtes et des menus
        cout_km : dictionnaire des listes de coûts, composées des éléments
        kilométriques
        """
        self.dico = dico
        self.cout_km = cout_km

        # Initialisation de l'affichage graphique
        self.root = Tk()
        self.root.title(dico['titre'][0])

        # Création d'un élément graphique permettant de dessiner à l'intérieur
        canevas = Canvas(self.root, width=int(dico['largeur']/4),
                         height=int(dico['hauteur']/4))

        try:
            # Chargement d'une image et affichage en position (0,0)
            logo_tux = PhotoImage(file="logo_tux.gif")
            img_logo = canevas.create_image(0, 0, anchor=NW, image=logo_tux)
            self.logo = True
        except TclError:
            self.logo = False

        # Création d'un cadre pour l'en-tête
        cadre = Frame(self.root, width=int(dico['largeur']/2),
                      height=int(dico['hauteur']/4))

        # Définition en-tête
        indice = 0
        for titre in dico['titre']:
            if indice == 0:
                Label(cadre, text=titre).pack(padx=10, pady=10)
                indice += 1
            else:
                Label(cadre, text=titre).pack()

        # Définition de la distance
        txt_distance = Label(self.root, text=dico['distance'])
        self.entr_distance = Entry(self.root)

        # Définition des modes de déplacement
        txt_dep_1 = Label(self.root, text=dico['deplacement'].format(dico[1]))
        txt_dep_2 = Label(self.root, text=dico['deplacement'].format(dico[2]))

        # Création des Listbox
        # l'option exportselection à False permet de ne pas exporter la
        # sélection au serveur X et ainsi de pouvoir gérer plusieurs Listbox
        self.list_dep_1 = Listbox(self.root, exportselection=False,
                                  height=len(dico['menu']))
        self.list_dep_2 = Listbox(self.root, exportselection=False,
                                  height=len(dico['menu']))
        for indice in range(len(dico['menu'])):
            self.list_dep_1.insert(indice, dico['menu'][indice])
        for indice in range(len(dico['menu'])):
            self.list_dep_2.insert(indice, dico['menu'][indice])
        # Par défaut, l'état des listes est désactivé
        self.list_dep_1.config(state=DISABLED)
        self.list_dep_2.config(state=DISABLED)

        # Création des labels pour les titres des coûts
        txt_titre_co2 = Label(self.root, text=cout_km['lab_titres'][0])
        txt_titre_ener = Label(self.root, text=cout_km['lab_titres'][1])

        # Création des labels pour les coûts du premier mode de déplacement
        self.txt_co2_dep_1 = Label(self.root, text="")
        self.txt_ener_dep_1 = Label(self.root, text="")

        # Création des labels pour les coûts du second mode de déplacement
        self.txt_co2_dep_2 = Label(self.root, text="")
        self.txt_ener_dep_2 = Label(self.root, text="")

        # Création des labels pour les comparaisons de coûts
        self.txt_comp_co2_1 = Label(self.root, text="")
        self.txt_comp_ener_1 = Label(self.root, text="")
        self.txt_comp_co2_2 = Label(self.root, text="")
        self.txt_comp_ener_2 = Label(self.root, text="")

        # Gestion des événements
        self.entr_distance.bind("<Button-1>", self.click)

        # Définition d'un bouton de validation
        btn_cal = Button(self.root, text=dico['valide'], command=self.calcule)

        # Mise en page de la fenêtre graphique
        canevas.grid(row=1, column=1, padx=10, pady=10)
        cadre.grid(row=1, column=2, columnspan=3, padx=10, pady=10)
        txt_distance.grid(row=2, column=1, columnspan=2, padx=10, pady=10)
        self.entr_distance.grid(row=2, column=3, padx=10, pady=10)

        txt_titre_co2.grid(row=3, column=2, padx=10, pady=10)
        txt_titre_ener.grid(row=3, column=3, padx=10, pady=10)

        txt_dep_1.grid(row=4, column=1, padx=10, pady=10)
        self.list_dep_1.grid(row=5, column=1, padx=10, pady=1)
        self.txt_co2_dep_1.grid(row=5, column=2, padx=10, pady=10)
        self.txt_ener_dep_1.grid(row=5, column=3, padx=10, pady=10)

        txt_dep_2.grid(row=6, column=1, padx=10, pady=10)
        self.list_dep_2.grid(row=7, column=1, padx=10, pady=1)
        self.txt_co2_dep_2.grid(row=7, column=2, padx=10, pady=10)
        self.txt_ener_dep_2.grid(row=7, column=3, padx=10, pady=10)

        btn_cal.grid(row=8, column=1, padx=10, pady=10)
        self.txt_comp_co2_1.grid(row=8, column=2, padx=10, pady=10)
        self.txt_comp_ener_1.grid(row=8, column=3, padx=10, pady=10)
        self.txt_comp_co2_2.grid(row=9, column=2, padx=10, pady=1)
        self.txt_comp_ener_2.grid(row=9, column=3, padx=10, pady=1)

        # Boucle évènementielle
        self.root.mainloop()

    def calcule_cout_km(self, les_couts, km):
        """
        Calcule les coûts de déplacement.

        les_couts : liste des coûts kilométriques pour 1 km, liés à
        un mode de déplacement
        km : distance entre le domicile et le lieu de travail

        retourne une liste des coûts calculés liés au mode de déplacement
        """
        res = list(range(len(les_couts)))
        indice = 0
        for un_cout in les_couts:
            res[indice] = un_cout * km
            indice = indice + 1
        return res

    def compare_cout_km(self, les_couts_1, les_couts_2):
        """
        Compare les coûts de déplacement.

        les_couts_1 : liste des coûts calculés pour le 1er mode de déplacement
        les_couts_2 : liste des coûts calculés pour le 2eme mode de déplacement

        retourne une liste contenant les coûts kilométriques comparés
        """
        diff = list(range(len(les_couts_1)))
        for indice in range(len(les_couts_1)):
            diff[indice] = les_couts_2[indice] - les_couts_1[indice]
        return diff

    def click(self, event):
        """
        Active l'état des listes de choix.

        event : objet décrivant l'événement ayant déclenché l'appel de
        la fonction

        Pas de valeur de retour
        """
        self.list_dep_1.config(state=NORMAL)
        self.list_dep_2.config(state=NORMAL)

    def calcule(self):
        """
        Calcule les coût kilométriques.

        Pas de valeur de retour
        """
        # Récupérer les valeurs à partir des widget :
        km = int(self.entr_distance.get())
        mode_dep_1 = self.list_dep_1.curselection()[0]
        mode_dep_2 = self.list_dep_2.curselection()[0]
        # calcule les coûts :
        cout_dep_1 = self.calcule_cout_km(self.cout_km[int(mode_dep_1)], km)
        cout_dep_2 = self.calcule_cout_km(self.cout_km[int(mode_dep_2)], km)
        diff_couts = self.compare_cout_km(cout_dep_1, cout_dep_2)
        # Mis à jour des textes :
        pre = 1
        self.txt_co2_dep_1.config(
            text=self.cout_km['lab_unites'][0].
            format(round(cout_dep_1[0], pre)))
        self.txt_ener_dep_1.config(
            text=self.cout_km['lab_unites'][1].
            format(round(cout_dep_1[1], pre)))
        self.txt_co2_dep_2.config(
            text=self.cout_km['lab_unites'][0].
            format(round(cout_dep_2[0], pre)))
        self.txt_ener_dep_2.config(
            text=self.cout_km['lab_unites'][1].
            format(round(cout_dep_2[1], pre)))
        if diff_couts[0] < 0:
            self.txt_comp_co2_1.config(
                text=self.cout_km['lab_dep_1'][0].
                format(abs(round(diff_couts[0], pre))))
            self.txt_comp_ener_1.config(
                text=self.cout_km['lab_dep_1'][1].
                format(abs(round(diff_couts[1], pre))))
            self.txt_comp_co2_2.config(text=self.cout_km['lab_dep_2'][0])
            self.txt_comp_ener_2.config(text=self.cout_km['lab_dep_2'][1])
        else:
            self.txt_comp_co2_1.config(
                text=self.cout_km['lab_eco_1'][0].
                format(round(diff_couts[0], pre)))
            self.txt_comp_ener_1.config(
                text=self.cout_km['lab_eco_1'][1].
                format(round(diff_couts[1], pre)))
            self.txt_comp_co2_2.config(text=self.cout_km['lab_eco_2'][0])
            self.txt_comp_ener_2.config(text=self.cout_km['lab_eco_2'][1])
